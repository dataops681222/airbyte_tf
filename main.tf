resource "airbyte_source_faker" "custom_faker" {
  configuration = {
    always_updated    = false
    count             = 3
    parallelism       = 9
    records_per_slice = 5
    seed              = 6
    source_type       = "faker"
  }
  name          = "Custom Faker"
  workspace_id  = "0e1c6152-f6b4-4546-933b-04408e926497"
}


resource "airbyte_destination_snowflake" "custom_faker_destination_snowflake" {
  configuration = {
    credentials = {
      destination_snowflake_authorization_method_username_and_password = {
        username = "AIRBYTE_USER"
        password = "password"
      }
    }
    destination_type      = "snowflake"
    database              = "AIRBYTE_DATABASE"
    disable_type_dedupe   = true
    host                  = "absbsdo-pq27240.snowflakecomputing.com"
    retention_period_days = 7
    role                  = "AIRBYTE_ROLE"
    schema                = "AIRBYTE_SCHEMA"
    username              = "AIRBYTE_USER"
    warehouse             = "AIRBYTE_WAREHOUSE"
    loading_method = {
      destination_snowflake_update_data_staging_method_recommended_internal_staging = {
        method = "Internal Staging"
      }
    }
  }
  name          = "SnowFlake destination"
  workspace_id  = "0e1c6152-f6b4-4546-933b-04408e926497"

}




resource "airbyte_connection" "custom_airbyte_snowflake_connection" {
  destination_id       = airbyte_destination_snowflake.custom_faker_destination_snowflake.destination_id
  source_id            = airbyte_source_faker.custom_faker.source_id
  name                 = "custom_faker_connection"
  status               = "active"
  configurations       = {
    streams = [
      {
        name = "purchases"
      }
    ]
  }
}
